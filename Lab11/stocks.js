window.onload = function() {
    document.getElementById("dump").onclick=function(){
    function stocks (company, MarketCap, Sales, profit, employees) {
      this.company = company;
      this.MarketCap = MarketCap;
      this.Sales = Sales;
      this.profit = profit;
      this.employees = employees;
    }
    
    
    var stocks = [
        new stocks('Microsoft','$381.7 B','$86.8 B','$22.1 B','128,000'),
        new stocks('Symetra Financial', '$2.7 B', '$2.2 B', '$254.4 M', '1,400'),
        new stocks('Micron Technology','$37.6 B','$16.4 B'),
        new stocks('F5 Networks','$9.5 B','$1.7 B','$311.2 M','3,834'),
        new stocks('Expedia','$10.8 B','$5.8 B','$274.4 M','18,210'),
        new stocks('Nautilus','$476 M','$274.4 M','$18.8 M','340'),
        new stocks('Heritage Financial','$531 M','$137.6 M','$21 M','748'),
        new stocks('Cascade Microtech','$239 M','$136 M','$9.9 M','449'),
        new stocks('Nike','$83.1 B','$27.8 B','$2.7 B','56,500'),
        new stocks('Alaska Air Group','$7.9 B','$5.4 B','$605 M','13,952')
        
    ];
    var html = '';
    stocks.forEach(function(a){
                   html += "<tr><td>"+a['company']+"</td><td>"+a['MarketCap']+"</td><td>"+a['Sales']+"</td><td>"+a['profit']+"</td><td>"+a['employees']+"</td></tr>";
    });
                document.getElementById('results').innerHTML = html;
}}