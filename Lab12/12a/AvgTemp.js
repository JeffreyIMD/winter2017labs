window.onload = function () {
    //document.getElementById('avg').innerHTML
    var high = [82, 75, 69, 69, 68];
    var low = [55, 52, 52, 48, 51];
 
    var total = high.reduce(function (a, b) {
                return a + b;
            });
    
    var highlength = high.length;
    var avghigh = total / highlength;

    var totalLow = low.reduce(function (a, b)
            {
                return a + b;
            });
    var lowlength = low.length;
    var avglow = totalLow / lowlength;
    
    var html = '';
    html += "<tr><td>" + avghigh + "</td><td>" + avglow + "</td><td></tr>";
    document.getElementById("avg").innerHTML = html;
    
};