//window.onload = function() {
function buildObject(url, teamName, abbr, conference, overall) {
  var myobj = {};
  myobj['logo'] = url;
  myobj['school'] = teamName;
  myobj['abbreviation'] = abbr;
  myobj['conference'] = conference;
  myobj['overall'] = overall;
  return myobj;
}

var pac12_north_logo = ["https://canvas.uw.edu/courses/1109459/files/40884847/preview", 
                       "https://canvas.uw.edu/courses/1109459/files/40884848/preview",
                       "https://canvas.uw.edu/courses/1109459/files/40884842/preview",
                       "https://canvas.uw.edu/courses/1109459/files/40884824/preview",
                       "https://canvas.uw.edu/courses/1109459/files/40884825/preview",
                       "https://canvas.uw.edu/courses/1109459/files/40884821/preview"];

var teams = [];
teams.push(buildObject(pac12_north_logo[0], 'Washington', 'UW', '7-1', '10-1'));
teams.push(buildObject(pac12_north_logo[1], 'Washington State', 'WSU', '7-1', '10-1'));
teams.push(buildObject(pac12_north_logo[2], 'Stanford', 'SU', '7-1', '10-1'));
teams.push(buildObject(pac12_north_logo[3], 'Oregon', 'OU', '7-1', '10-1'));
teams.push(buildObject(pac12_north_logo[4], 'Oregon State', 'OSU', '7-1', '10-1'));
teams.push(buildObject(pac12_north_logo[5], 'California', 'CAL', '7-1', '10-1'));











// }

//};